//
//  ArticleDetailsViewController.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

class ArticleDetailsViewController: UIViewController {
    private var article: Article?
    
    @IBOutlet private weak var articleImage: UIImageView!
    @IBOutlet private weak var articleSource: UILabel!
    @IBOutlet private weak var articleTitle: UILabel!
    @IBOutlet private weak var articleAuthor: UILabel!
    @IBOutlet private weak var articleDate: UILabel!
    @IBOutlet private weak var articleAbstract: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let article else { return }
        
        let bestImage = chooseBestMedia(media: article.media)
        if let bestImage {
            let url = URL(string: bestImage.url)
            if let url {
                let networkManager = NetworkManager(url: url)
                networkManager.obtainImage { [weak self] result in
                    switch result {
                    case .success(let image):
                        self?.articleImage.image = image
                    case .failure(_):
                        break
                    }
                }
            }
        }
        
        articleSource.text = article.source
        articleTitle.text = article.title
        articleAuthor.text = article.author
        articleDate.text = article.date
        articleAbstract.text = article.abstract
    }
    
    func articleToShow(_ article: Article) {
        self.article = article
    }
    
    private func chooseBestMedia(media: [Media]) -> MediaMetadata? {
        let packsOfImages = media.filter { $0.type == "image" }
        
        var bestImages = [MediaMetadata?]()
        for images in packsOfImages {
            bestImages.append(images.metadatas.max { $0.height*$0.width < $1.height*$1.width })
        }
        
        return bestImages.compactMap { $0 }.max { $0.height*$0.width < $1.height*$1.width }
    }
}
