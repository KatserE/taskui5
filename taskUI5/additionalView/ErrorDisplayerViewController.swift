//
//  ErrorDisplayerViewController.swift
//  taskUI5
//
//  Created by Евгений Кацер on 22.05.2023.
//

import UIKit

class ErrorDisplayerViewController: UIViewController {
    private var errorMsg: String?
    @IBOutlet private weak var error: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        error.text = errorMsg
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        exit(0)
    }
    
    func messageToShow(_ errorMsg: String) {
        self.errorMsg = errorMsg
    }
}
