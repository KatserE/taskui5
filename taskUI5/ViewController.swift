//
//  ViewController.swift
//  taskUI5
//
//  Created by Евгений Кацер on 20.05.2023.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private static let detailInfoID = "detailInfoID"
    private static let errorDisplayerID = "errorDisplayerID"
    
    @IBOutlet private weak var tableView: UITableView!
    
    private var networkManager: NetworkManager?
    private var articles = [Article]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(cell: ArticleTableViewCell.self)
        
        var url: URL = URL.picturesDirectory
        do {
            url = try URLMaker.makeURL(request: "https://api.nytimes.com/svc/mostpopular/v2/viewed/30.json")
        } catch URLMakerError.CantLoadAPIKey { // If go to any catch => exit(0)
            wasOccuredError("Can't find a file with the api-key!")
        } catch URLMakerError.UncorrectURL {
            wasOccuredError("Can't connect to server!")
        } catch {
            wasOccuredError("Something wrong!")
        }
        
        networkManager = NetworkManager(url: url)
        networkManager!.obtainArticles { [weak self] result in
            switch result {
            case .success(let recivedArticles):
                self?.articles = recivedArticles
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    let errorMsg: String
                    
                    switch error {
                    case .CantConnectToServer: errorMsg = "Can't connect to server!"
                    case .ServerDontSendData: errorMsg = "Server didn't send data!"
                    case .CantParseData: errorMsg = "Can't parse data!"
                    }
                    
                    self?.wasOccuredError(errorMsg)
                }
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: ArticleTableViewCell.getID(),
            for: indexPath
        ) as! ArticleTableViewCell
        
        let article = articles[indexPath.row]
        cell.configure(title: article.title, author: article.author)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = articles[indexPath.row]
        performSegue(withIdentifier: ViewController.detailInfoID, sender: article)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.detailInfoID, let article = sender as? Article {
            let destination = segue.destination as! ArticleDetailsViewController
            destination.articleToShow(article)
        } else if segue.identifier == ViewController.errorDisplayerID, let errorMsg = sender as? String {
            let destination = segue.destination as! ErrorDisplayerViewController
            destination.messageToShow(errorMsg)
        }
    }
    
    private func wasOccuredError(_ errorMsg: String) {
        performSegue(withIdentifier: ViewController.errorDisplayerID, sender: errorMsg)
    }
}

