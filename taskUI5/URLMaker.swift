//
//  URLMaker.swift
//  taskUI5
//
//  Created by Евгений Кацер on 22.05.2023.
//

import Foundation

enum URLMakerError : Error {
    case CantLoadAPIKey
    case UncorrectURL
}

final class URLMaker {
    static func makeURL(request: String, apikey: String = "") throws -> URL {
        let url = URL(string: request + "?api-key=\(apikey)")
        guard let url else { throw URLMakerError.UncorrectURL }
        
        return url
    }
}
