//
//  UITableViewExtensions.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

extension UITableView {
    func register(cell: CustomCell.Type) {
        register(cell.getNib(), forCellReuseIdentifier: cell.getID())
    }
}
