//
//  NetworkManager.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

import UIKit

enum NetworkManagerObtainError : Error {
    case CantConnectToServer
    case ServerDontSendData
    case CantParseData
}

enum ObtainArticlesResult {
    case success(articles: [Article])
    case failure(error: NetworkManagerObtainError)
}

enum ObtainImageResult {
    case success(image: UIImage)
    case failure(error: NetworkManagerObtainError)
}

final class NetworkManager {
    private let session: URLSession
    private let url: URL
    private let decoder = JSONDecoder()
    
    init(url: URL) {
        session = URLSession.shared
        self.url = url
    }
    
    func obtainArticles(completion: @escaping (ObtainArticlesResult) -> ()) {
        session.dataTask(with: url) { [weak self] data, response, error in
            var result: ObtainArticlesResult
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            guard error == nil else {
                result = .failure(error: NetworkManagerObtainError.CantConnectToServer)
                return
            }
            
            if let data {
                let parsed = try? self?.decoder.decode(ApiResponse.self, from: data)
                guard let parsed else {
                    result = .failure(error: NetworkManagerObtainError.CantParseData)
                    return
                }
                
                result = .success(articles: parsed.articles)
            } else {
                result = .failure(error: NetworkManagerObtainError.ServerDontSendData)
            }
        }.resume()
    }
    
    func obtainImage(completion: @escaping (ObtainImageResult) -> ()) {
        let data = try? Data(contentsOf: url)
        var result: ObtainImageResult
        defer {
            DispatchQueue.main.async {
                completion(result)
            }
        }
        
        guard let data else {
            result = .failure(error: NetworkManagerObtainError.CantConnectToServer)
            return
        }
        
        guard let image = UIImage(data: data) else {
            result = .failure(error: NetworkManagerObtainError.CantParseData)
            return
        }
        
        result = .success(image: image)
    }
}
