//
//  Media.swift
//  taskUI5
//
//  Created by Евгений Кацер on 22.05.2023.
//

final class MediaMetadata : Codable {
    let url: String
    let height: Int
    let width: Int
}

final class Media : Codable {
    let type: String
    let metadatas: [MediaMetadata]
    
    enum CodingKeys: String, CodingKey {
        case type
        case metadatas = "media-metadata"
    }
}
