//
//  Article.swift
//  taskUI5
//
//  Created by Евгений Кацер on 21.05.2023.
//

final class Article : Codable {
    let source: String
    let date: String
    let section: String
    let author: String
    let title: String
    let abstract: String
    let media: [Media]
    
    enum CodingKeys: String, CodingKey {
        case source
        case date = "published_date"
        case section
        case author = "byline"
        case title
        case abstract
        case media
    }
}
